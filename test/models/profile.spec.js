'use strict';
/*jshint expr: true*/

var expect = require('chai').expect;
var _ = require('lodash');

var dataFactory = {
  create: function (overwrites) {
    var defaults = {
      email: "raul.osvaldo.robledo@gmail.com",
      googleId: "12345678",
      displayName: "Raul Robledo",
      gender: "male",
      firstName: "Raul",
      lastName: "Robledo"
    };
    return _.extend(defaults, overwrites);
  }
};

describe('Profile', function () {

  var Profile, data;

  beforeEach(function () {
    Profile = require('../../src/models/Profile');
  });

  it('should have every attribute', function () {
    data = dataFactory.create();
    Profile.create(data, function (err, Profile) {
      expect(err).to.be.null;
      expect(Profile.email).to.equal('raul.osvaldo.robledo@gmail.com');
      expect(Profile.googleId).to.equal('12345678');
      expect(Profile.displayName).to.equal('Raul Robledo');
      expect(Profile.gender).to.equal('male');
    });
  });


  it('should give an error when email is missing', function() {
    Profile.create({}, function (err, Profile) {
      expect(Profile).to.not.be.ok;
      expect(err).to.match(/email is required!/);
    });
  });

  it('should have #toJSON to get clean json', function () {
    data = dataFactory.create();
    Profile.create(data, function (err, Profile) {
      expect(err).to.be.null; 
      expect(Profile.toJSON()).to.have.property('id');
      expect(Profile.toJSON()).to.not.have.property('_id');
      expect(Profile.toJSON()).to.not.have.property('__V');
    });
  });

});
# testservice

Steps to create docker package:

```
docker build . -t testservice 
docker run -p 5000:5000 -it testservice
```

Steps to push package:

```
docker login
docker tag testservice rrobledo/testservice
docker push rrobledo/testservice
```

/**
 * configuration file
 *
 */

'use strict';

var _ = require('lodash');

var config = (function (env) {
  var conf = {};

  // Common configuration
  conf.common =  {
    app: {
      name: "rest-express"
    },
    port: process.env.PORT || 5000,
    mongoURI: process.env.MONGO_URI || "mongodb://root:example@127.0.0.1:27017/testservice?authSource=admin",
  };

  // Development configuration
  conf.development = {
    env: "development",
  };

  // Test configuration
  conf.test = {
    env: "test",
    port: process.env.PORT || 3030,
  };

  // Production configuration
  conf.production = {
    env: "production",
  };

  return _.merge(conf.common, conf[env]);

})(process.env.NODE_ENV || 'development');

module.exports = config;
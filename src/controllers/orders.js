const logger = require('../lib/logger')
const shopify = require('./shopify')

function Order() {
  /**
   * Create a new order
   * @param {email} email user logged
   * @param {order} List of item to buy
   */
  const create = (email, order) => {
    return new Promise((resolve, reject) => {
      validateVariants(order)
        .then(isValid => {
          if (!isValid) {
            reject("There are invalid variants ids or some one is out of stock");
          } else if (!email) {
            reject("The logged user is invalid");
          } else {
            let listItems = order.items.map(item => {
                                              return {
                                                variant_id : item.variantId,
                                                quantity : item.quantity,
                                              };
                                            });
            let newOrder = {
                "email": email,
                "line_items": listItems
              }

            shopify.order.create(newOrder)
              .then(orderCreated => {
                resolve(orderCreated);
              })
              .catch(error => reject(error))
          }
        })
        .catch(error => reject(error))
    })
  }

  /**
   * Get orders by user
   * @param {email} email user logged
   */
  const getByEmail = (email) => {
    return new Promise((resolve, reject) => {
      /*
      This implementation is not working since the token that I'm using does not have the read_customers scope,
      so every time that I'm getting the customer by id I'm receiving the following error.
      {
        "errors": "[API] This action requires merchant approval for read_customers scope."
      }

      ---------------------------------------------------
      shopify.customer.list({email : email})
        .then(customer => {
          if (customer) {
            shopify.order.list({customer_id : customer.id})
            .then(orders => resolve(orders))
            .catch(error => reject(error))
          } else {
            reject("The logged user is invalid");
          }
        })
        .catch(error => reject(error))
      ---------------------------------------------------

      Based on last, I have implemented the filter on applications side, take in account that it has several performance problems.
      */
     shopify.order.list()
      .then(orders => {
        resolve(orders.filter(order => order.email == email))
      })
      .catch(error => reject(error))
    });
  }

  /**
   * Cancel an order
   * @param {orderId} order Id
   */
  const cancel = (orderId) => {
    return new Promise((resolve, reject) => {
     shopify.order.cancel(orderId)
      .then(order => {
        resolve()
      })
      .catch(error => reject(error))
    });
  }

  /**
   * Validate whether variants ids are valid and their exists on stock
   * @private
   * @param {order} order 
   */
  const validateVariants = (order) => {
    return new Promise((resolve, reject) => {
      let variantsResult = order.items.map(item => {
                            return shopify.productVariant.get(item.variantId);
                          });
  
      Promise.all(variantsResult)
        .then(variants => {
          let isValid = true;
          variants.forEach(variant => {
              isValid = isValid && (variant !== undefined);
          });
          resolve(isValid)
        })
        .catch(error => reject(error))
    });
  }

  return {
    create: create,
    getByEmail: getByEmail,
    cancel: cancel
  }
}

module.exports = Order();

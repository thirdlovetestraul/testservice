const logger = require('../lib/logger')
const Cart = require('../models/Cart');
const shopify = require('./shopify')

/**
 * Add Items to user cart
 * 
 * @param {profileId} profileId's car
 * @param {item} item to add into cart
 */
module.exports.AddItemToCart = (profileId, item) => {
  return new Promise((resolve, reject) => {
    shopify.productVariant.get(item.variantId)
      .then(variant => {
        if (variant && variant.product_id === item.productId ) {
          let options = item.options.map( option => {
                                            return {
                                                    id: option.id,
                                                    value: option.value
                                                    };
                                          });
          let cart = new Cart({
              profileId: profileId,
              productId: item.productId,
              variantId: item.variantId,
              quatity: item.quatity,
              options: options
          });
          cart.save()
            .then(saveCart => resolve(saveCart))
            .catch(error => reject(error))
        } else if (!variant) {
          reject("Variant Not Found")
        } else {
          reject("Product doesn't match with variant")
        }
      })
      .catch(error => reject(error));
  })
}

/**
 * Get user cart
 * 
 * @param {profileId} profileId's car
 */
module.exports.getCart = (profileId) => {
  return Cart.find({profileId : profileId})
}

/**
 * Remove Item from cart's user
 * 
 * @param {profileId} profileId's car
 * @param {item} item to add into cart
 */
module.exports.removeItemFromCart = (itemId) => {
  return new Promise((resolve, reject) => {
    Cart.findById(itemId)
      .then(item => {
        if (item) {
          Cart.findByIdAndRemove(itemId)
            .then(ret => resolve(ret))
            .catch(error => reject(error))
        } else {
          reject("Item Not found")
        }
      })
      .catch(error => reject(error));
  });
}

const exceptions = require('../../lib/exceptions')
const express = require('express');
const router = express.Router();
const ordersController = require('../../controllers/orders')
const validate = require('express-jsonschema').validate;

/**
 * Schema Definition used to validate http body
 */
const OrderSchema = {
        type: 'object',
        properties: {
            items: {
                type: 'array',
                required: true,
                items: {
                  type: 'object',
                  properties: {
                    variantId: {
                      type: 'number',
                      required: true
                    },
                    quantity: {
                      type: 'number',
                      required: true
                    },
                  }
                }
            }
        }
      }

/**
 * POST: /shopping/orders
 */
router.post('/', validate({body: OrderSchema}), function(req, res, next) {
  ordersController.create(req.email, req.body)
    .then(order => {
      res.send({
        data: order}
      );
    })
    .catch(error => {
      next(new exceptions.MissingArgument(error))
    })
});

/**
 * GET: /shopping/orders
 */
router.get('/', function(req, res, next) {
  ordersController.getByEmail(req.email)
    .then(orders => {
      res.send({
        data: orders}
      );
    })
    .catch(error => {
      next(new exceptions.DataNotFound(error))
    })
});

/**
 * PUT: /shopping/orders/{orderId}/cancel
 */
router.put('/:id/cancel', function(req, res, next) {
  ordersController.cancel(req.params.id)
    .then(items => {
      res.send('Canceled successfully!');
    })
    .catch(error => {
      next(new exceptions.DataNotFound(error))
    })
});

module.exports = router;

const express = require('express');
const router = express.Router();

const products = require('./products.js');
const cart = require('./cart.js');
const orders = require('./orders.js');

router.use('/products', products);
router.use('/cart', cart);
router.use('/orders', orders);

module.exports = router;

const exceptions = require('../../lib/exceptions')
const express = require('express');
const shopify = require('../../controllers/shopify')
const router = express.Router();


router.get('/', (req, res, next) => 
{
  let page = (req.query.page ? req.query.page : 1)
  let limit = (req.query.limit ? req.query.limit : 50)

  shopify.product.list({ limit: limit, page: page })
    .then(products => {
      res.send({
        data : products
      });  
    })
    .catch(error => {
      next(new exceptions.DataNotFound(error))
    })
})

router.get('/:id/variants', (req, res, next) => 
{
  let productId = req.params.id
  let page = (req.query.page ? req.query.page : 1)
  let limit = (req.query.limit ? req.query.limit : 50)

  shopify.productVariant.list(productId, { limit: limit, page: page })
    .then(variants => {
      res.send({
        data : variants
      });  
    })
    .catch(error => {
      next(new exceptions.DataNotFound(error))
    })
})

module.exports = router;

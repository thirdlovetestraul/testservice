const exceptions = require('../../lib/exceptions')
const express = require('express');
const router = express.Router();
const cartController = require('../../controllers/cart')
const validate = require('express-jsonschema').validate;

/**
 * Schema Definition used to validate http body
 */
const CartSchema = {
        type: 'object',
        properties: {
            productId: {
              type: 'number',
              required: true
            },
            variantId: {
              type: 'number',
              required: true
            },
            quatity: {
                type: 'number',
                required: true
            },
            options: {
                type: 'array',
                required: true,
                items: {
                  type: 'object',
                  properties: {
                    id: {
                      type: 'string',
                      required: true
                    },
                    value: {
                      type: 'string',
                      required: true
                    },
                  }
                }
            }
        }
      }

/**
 * POST: /shopping/cart
 */
router.post('/', validate({body: CartSchema}), function(req, res, next) {
  cartController.AddItemToCart(req.profileId, req.body)
    .then(item => {
      res.send({
        data: item}
      );
    })
    .catch(error => {
      next(new exceptions.DataNotFound(error))
    })
});

/**
 * GET: /shopping/cart
 */
router.get('/', (req, res, next) => {
  cartController.getCart(req.profileId)
    .then(items => {
      res.send({
        data: items}
      );
    })
    .catch(error => {
      next(new exceptions.InternalServerError(error))
    })
})

/**
 * DELETE: /shopping/cart
 */
router.delete('/:id', (req, res, next) => {
  cartController.removeItemFromCart(req.params.id)
    .then(items => {
      res.send('Deleted successfully!');
    })
    .catch(error => {
      next(new exceptions.DataNotFound(error))
    })
})

module.exports = router;

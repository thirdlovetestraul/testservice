const logger = require('../lib/logger');
const express = require('express');
const router = express.Router();

const shopping = require('./shopping');

router.use('/shopping', shopping);

module.exports = router;

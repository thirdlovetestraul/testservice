const passport = require('passport');
const logger = require('../lib/logger')
const jwt = require('jsonwebtoken');
const exceptions = require('../lib/exceptions')

let JWT_SECRET = "secret"

let getToken = (req) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0].toUpperCase() === 'BEARER') {
      return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
}

passport.validAuthentication = (options) => {
  return (req, res, next) => {
    if (options.excludes 
        && options.excludes.reduce((acum, item) => {return acum + (req.url.includes(item) ? 1 : 0) }, 0) > 0) {
      next()
    } else {
      let token = getToken(req);
      if (token) {
        try {
          let decoded = jwt.verify(token, JWT_SECRET);
          req.profileId = decoded.profileId
          req.email = decoded.email
          next()
        } catch(err) {
          next(new exceptions.Unauthorized("Invalid JWT Token"));
        }
      } else {
        next(new exceptions.Unauthorized("JWT Token need to be provided"));
      }
      
    }
  }
}

module.exports = passport
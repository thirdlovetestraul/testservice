'use strict';

var methodOverride = require('method-override');
var errorHandler = require('./lib/errorHandler.js');
var bodyParser = require('body-parser');
var express = require('express');
var session = require('express-session')
var morganLogger  = require('morgan'); // HTTP request logger
var config = require('./config');
var routes = require('./routes');
var passport = require('./service/passport');
const mongoose = require('mongoose');
require('./models/Cart.js');

var app = express();

var options = { 
  server: {
    socketOptions: {
      keepAlive: 1
    },
    reconnectTries: 30
  }
};
mongoose.connect(config.mongoURI, options);

app.use(express.static("public"));
app.use(session({ 
                  secret: "testServerThridLove" , 
                  resave: false,
                  saveUninitialized: true,
                  cookie: { 
                    secure: true 
                  }
                }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
if (config.env === 'development') {
  app.use(morganLogger('dev'));
}
app.use(passport.initialize());
app.use(passport.session());
app.use(passport.validAuthentication({ 
                  excludes : ["/identity/auth/google",
                              "/shopping/products"]
                }))
app.use(routes);
app.use(errorHandler());


module.exports = app;
'use strict';
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const CartSchema = new Schema({
  profileId: { type: String },
  productId: { type: Number },
  variantId: { type: Number },
  quatity: { type: Number },
  options: [ 
            {
              id: { type: String },
              value: { type: String }
            }
          ]
});

CartSchema.index({ profileId: 1, score: -1 });

module.exports = mongoose.model('Cart', CartSchema);

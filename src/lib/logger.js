const log4js = require('log4js');

log4js.configure({
  appenders: { myapp: { type: 'file', filename: '/shared/logs/identity.log' } },
  categories: { default: { appenders: ['myapp'], level: 'info' } },
});
const logger = log4js.getLogger('identity');

module.exports = logger;

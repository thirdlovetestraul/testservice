'use strict';
const exceptions = require('./exceptions')

var getError = function(status, message, code, aboutLink) {
  return {
    status: status,
    message: message,
    code: code,
    link: {
      about: aboutLink
    }
  }
}

var errorHandler = function () {
  return function (err, req, res, next) {

    if (err.name === 'JsonSchemaValidation') {
      res.status(400)
      .json(getError(400, 
        err.validations, 
        err.code,
        err.aboutLink));
    } else {
      switch (err.constructor) {
        case exceptions.Unauthorized:
          res.status(401)
          .json(getError(401, 
                            err.message, 
                            err.code,
                            err.aboutLink));
          break; 
        case exceptions.MissingArgument:
          res.status(400)
          .json(getError(400, 
                            err.message, 
                            err.code,
                            err.aboutLink));
          break; 
        case exceptions.DataNotFound:
          res.status(404)
          .json(getError(404, 
                            err.message, 
                            err.code,
                            err.aboutLink));
          break; 
        case exceptions.InternalServerError:
          res.status(500)
          .json(getError(400, 
                            err.message, 
                            err.code,
                            err.aboutLink));
          break; 
        default:
          res.status(500)
          .json(getError(500, 
                            err.message, 
                            err.code ? err.code : "5000",
                            err.aboutLink ? err.aboutLink : "http://www.domain.com/about/5000"));
      }
    }
  };
};

module.exports = errorHandler;
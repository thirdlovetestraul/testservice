FROM node:8.12-alpine

WORKDIR /app

ADD src /app
COPY package.json /app
RUN cd /app && npm install

CMD ["node", "/app/index.js"]
